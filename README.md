## Summary

Project created to hold some plotting programs related to probability.


## Programs

    # create (and run) gnuplot program to plot beta distribution mixture model.
    % perl betaDistMixture_gnuplot.pl 0.8 5 5 0.2 8


## Requirements

perl, gnuplot
