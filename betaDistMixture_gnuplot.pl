#!/usr/bin/perl -w
#  Author: Paul Horton
#  Copyright (C) 2020, Paul Horton, All rights reserved.
#  Created: 20201106
#  Updated: 20201106
#
#  Description:  Generate (and run) a gnuplot script to plot a beta distribution mixture model.
#
#  Context:  I wrote this while teaching a course introducing probabilistic inference. PH20201106
#
use autodie qw(:all);
use Scalar::Util qw(looks_like_number);
use Getopt::Std qw(getopts);
use strict;



#━━━━━━━━━━━━━━━━━━━━━━━━━  Process ARGV  ━━━━━━━━━━━━━━━━━━━━━━━━━
my $usage=<<"__USAGE__";
Usage:
$0 [flags]   a1 b1         #plots  BetaDist(a1,b2)
$0 [flags] m a1 b1 a2 b2   #plots mixture  m*BetaDist(a1,b1) + (1-m)*BetaDist(a2,b2)
flags:
     -f  make gnuplot save plot as file named like: (plots/)betaDistMix:m__a1_b1__a2_b2[H].png
     -H  hide plot title and mixture components
     -n  just print gnuplot script to STDOUT
Examples: $0 0.9 8 8 8 0.2
          $0 0.5 0.5
__USAGE__


die "$usage\n"    if  !@ARGV or $ARGV[0]=~/^--?[hu]/;  #covers -help, --usage etc.

getopts 'fHns', \my %opt;


my $mixP=
  do{ my $numComponents= (undef, 1, undef, undef, 2)[$#ARGV]   or   die  "Error.  Wrong number of arguments\n$usage";
      $numComponents > 1 };


sub finiteNumberP{   $_[0]!~/n/i  and  looks_like_number $_[0]};

my $m= $mixP?  shift : 1;
finiteNumberP $m     or   die  "Expected number for a parameter but got '$m'\n$usage";
0 <= $m && $m <= 1   or   die  "Expected mixture coefficient to be a value in [0-1], but got '$m'\n$usage";


my( $a1, $b1 )=  splice @ARGV, 0, 2;
my( $a2, $b2 )=  $mixP?  splice( @ARGV, 0, 2 ) : (1,1);


for  my $name(  qw(a1 b1 a2 b2)){
    my $val=  eval "\$$name";
    finiteNumberP $val   or   die  "Expected numerical value for $name, but got '$val'\n$usage";
    $val > 0             or   die  "Value of $name should be positive but got '$val'\n$usage";
}



#━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━  Gnuplot Program Head  ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
my $gpProg =<<__GNUPLOT__;
BetaPDF(x,a,b)= x**(a-1) * (1-x)**(b-1) * gamma(a+b) / gamma(a) / gamma(b)
BetaMixPDF(x,m,a1,b1,a2,b2)= m * BetaPDF(x,a1,b1) + (1-m) * BetaPDF(x,a2,b2)

set samples 1024
set xlabel "rate"
set ylabel "probability density"

set xrange [0:1]
set yrange [0:10]
set xtics 0.1,0.1,0.9
set arrow from 0.5, graph 0 to 0.5, graph 0.9 nohead dt '.'
__GNUPLOT__


if(  $opt{f}  ){
    my $filename= $mixP?
      "betaDistMix:${m}__${a1}_${b1}__${a2}_${b2}" :
      "betaDist:${a1}_${b1}";
    $filename=  "plots/$filename"    if -d 'plots';
    $filename.= 'H'                  if $opt{H};
    $gpProg.=  qq{set terminal png\nset output "$filename.png"\n};
}



#━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━  Gnuplot Plot Command  ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
if(  $mixP  ){
    $gpProg.= qq{BetaMixPDF(x,m,a1,b1,a2,b2)= m * BetaPDF(x,a1,b1) + (1-m) * BetaPDF(x,a2,b2)
plot BetaMixPDF(x,$m,$a1,$b1, $a2,$b2)  lw 2  };

    $gpProg.=
      $opt{H}? "notitle\n"
      :
      qq{ti "$m*Beta($a1,$b1) + @{[1-$m]}*Beta($a2,$b2)",   BetaPDF(x,$a1,$b1)  lw 1  dt '-'  ti "Beta($a1,$b1)",  BetaPDF(x,$a2,$b2)  lw 1  dt '-'  ti "Beta($a2,$b2)"\n};
}
else{#  Just one component.
    $gpProg.=  "plot BetaPDF(x,$a1,$b1)  lw 2  ";
    $gpProg.= ($opt{H}?  'notitle'  :qq{ti "Beta($a1,$b1)"})  .  "\n";
}




#━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━  Run or Print Gnuplot Program  ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
if(  $opt{n}  ){  print $gpProg; exit 1};


open my $GP, '|-',  'gnuplot'.($opt{f}? '' : ' -p');
print $GP $gpProg;

close $GP;
